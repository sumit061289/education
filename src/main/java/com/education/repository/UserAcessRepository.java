package com.education.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.education.model.User;
import com.education.model.UserAccess;

@Repository
public interface UserAcessRepository extends JpaRepository<UserAccess, Long> {
	
	Optional<UserAccess> findByUser(User user);
	
	void deleteByUser(User user); 
}
