package com.education.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.education.model.City;
import com.education.model.State;

public interface CityRepository extends JpaRepository<City, Long>{
	
	List<City> findCitiesByState(State state);

}
