package com.education.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.education.model.City;
import com.education.model.State;
import com.education.repository.CityRepository;
import com.education.repository.StateRepository;

@Service
public class DataService {
	
	@Autowired
	private StateRepository stateRepository;
	
	private CityRepository cityRepository; 

	public List<State> getAllStates() {
		List<State> allStates =  stateRepository.findAll();
		return allStates.stream().sorted(Comparator.comparing(State::getStateName)).collect(Collectors.toList());
		
	}
	
	public List<City> getAllCities(State state) {
		List<City> allCities = cityRepository.findCitiesByState(state);
		return allCities.stream().sorted(Comparator.comparing(City::getCityName)).collect(Collectors.toList());
		
	}
}
