package com.education.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.education.model.User;
import com.education.model.UserAccess;
import com.education.repository.UserAcessRepository;

@Service
public class UserAcessService {

	@Autowired
	private UserAcessRepository userAcessRepository;
	
	public UserAccess create(UserAccess userAccess) {
		return userAcessRepository.save(userAccess);
	}
	
	public Optional<UserAccess> findByUser(User user) {
		return  userAcessRepository.findByUser(user);
	}
	
	public void deleteByUser(User user) {
		userAcessRepository.deleteByUser(user);
	}
}
