package com.education.model;

public enum UserType {

	ENTERPRISEUSER,
	NORMALUSER;
}
