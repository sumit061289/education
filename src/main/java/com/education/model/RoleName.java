package com.education.model;

public enum RoleName {
	ROLE_STUDENT,
    ROLE_COLLEGE,
    ROLE_ADMIN
}
