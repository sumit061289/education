package com.education.model;

public class AcessResponse {
	private String accessToken;
	private long expiresIn;

	public AcessResponse() {
		// Empty Constructor
	}

	public AcessResponse(final String accessToken, final long expiresIn) {
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(final long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
