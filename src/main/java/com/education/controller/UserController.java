package com.education.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.education.config.JwtTokenUtil;
import com.education.model.AcessResponse;
import com.education.model.User;
import com.education.model.UserAccess;
import com.education.service.UserAcessService;
import com.education.service.UserService;


@RestController
@RequestMapping("/user")
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private UserAcessService userAcessService;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@GetMapping("/names")
	public ResponseEntity<List<String>> getNames(){
		List<String> test = new ArrayList<>();
		test.add("Sumit");
		test.add("Sumit1");
		return new ResponseEntity<>(test, HttpStatus.OK);
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> create(@RequestBody User user) throws Exception{
		  Optional<User> findUserByEmail = userService.findUserByEmail(user.getEmail());
		  if(findUserByEmail.isPresent()) {
			throw new Exception("User already Exist");
		  }
		User create = userService.create(user);
		return ResponseEntity.ok(create);
	}
	
	@PostMapping("/login")
	public AcessResponse login(@RequestBody User user) throws Exception {
		try {
			authenticationManager.authenticate(
							new UsernamePasswordAuthenticationToken(
									user.getEmail(),user.getPassword()));	
			

			final UserDetails userDetails = userService.loadUserByUsername(user.getEmail());
			Optional<User> userObject = userService.findUserByEmail(user.getEmail());
			final AcessResponse acessToken = jwtTokenUtil.generateToken(userDetails);
			Optional<UserAccess> userAcess = userAcessService.findByUser(userObject.get());
			UserAccess userAccess;
			if(userAcess.isPresent()) {
				
			} else {
				userAccess = new UserAccess();
				userAccess.setAcessToken(acessToken.getAccessToken());
				userAccess.setLoggedIn(true);
				userAccess.setUser(userObject.get());
				userAcessService.create(userAccess);
			}
			
			
			return acessToken;
		}catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	@PostMapping("/logout")
	public void logout(@RequestBody User user) throws Exception {
		
		Optional<User> userObject = userService.findUserByEmail(user.getEmail());
		userAcessService.deleteByUser(userObject.get());
		
	}
	
}
