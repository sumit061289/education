package com.education.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.education.model.City;
import com.education.model.State;
import com.education.service.DataService;

@RestController
@RequestMapping("/data/v1")
public class DataController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private DataService dataService;
	
	@GetMapping(value="/states",
					produces = { MediaType.APPLICATION_JSON_VALUE }
			)
	public List<State> getStateList(){
		List<State> allStates = dataService.getAllStates();
		return allStates;
	}
	
	@GetMapping("/city/{stateId}")
	public List<City> getCityList(@PathVariable("stateId") Long stateId) {
		State state = new State(stateId);
		List<City> allCities = dataService.getAllCities(state);
		return allCities;
		
	}
}
